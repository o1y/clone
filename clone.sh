main() {
  # Use colors, but only if connected to a terminal, and that terminal
  # supports them.
  if which tput >/dev/null 2>&1; then
      ncolors=$(tput colors)
  fi
  if [ -t 1 ] && [ -n "$ncolors" ] && [ "$ncolors" -ge 8 ]; then
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    YELLOW="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    BOLD="$(tput bold)"
    NORMAL="$(tput sgr0)"
  else
    RED=""
    GREEN=""
    YELLOW=""
    BLUE=""
    BOLD=""
    NORMAL=""
  fi

  # Only enable exit-on-error after the non-critical colorization stuff,
  # which may fail on systems lacking tput or terminfo
  set -e

  if [ ! -n "$OUT" ]; then
    OUT=$(pwd)/mesieh_repos
  fi

  if [ -d "$OUT" ]; then
    printf "${YELLOW}You already have some files cloned. ${NORMAL}\n"
    printf "You'll need to remove $OUT if you want to run this script again.\n"
    exit
  fi

  hash curl >/dev/null 2>&1 || {
    echo "Error: curl is not installed"
    exit 1
  }

  KEY_URL="https://bitbucket.org/!api/2.0/snippets/o1y/A9ApX/0d8f0b8d570cb6d67019ccfb0d1d0beac202f8ad/files/170614_bitbucket_read_only"

  env curl -s --create-dirs $KEY_URL -o $OUT/key || {
    printf "Error: retrieving key failed\n"
    exit 1
  }

  env chmod 0600 $OUT/key || {
    printf "Error: cannot change permissions 0600 for $OUT/key\n"
    exit 1
  }

  printf "${BLUE}Cloning Repositories...${NORMAL}\n"
  hash git >/dev/null 2>&1 || {
    echo "Error: git is not installed"
    exit 1
  }
  # The Windows (MSYS) Git is not compatible with normal use on cygwin
  if [ "$OSTYPE" = cygwin ]; then
    if git --version | grep msysgit > /dev/null; then
      echo "Error: Windows/MSYS Git is not supported on Cygwin"
      echo "Error: Make sure the Cygwin git package is installed and is first on the path"
      exit 1
    fi
  fi

  REPOS="linius
  stadium-vision
  radialsystem"

  for repo in $REPOS
  do
    env GIT_SSH_COMMAND="ssh -i $OUT/key" git clone --depth=1 git@bitbucket.org:o1y/$repo.git $OUT/$repo || {
      printf "Error: git clone of $repo repo failed\n"
      exit 1
    }
  done

  rm $OUT/key

  printf "${GREEN}"
  echo 'All repositories have been successfully cloned'
  printf "${NORMAL}"
}

main
